epita-env
=========

Helper to get configuration values from the environment.

## Installation instructions

```sh
$ pip install epita-env --index-url https://gitlab.cri.epita.fr/api/v4/projects/592/packages/pypi/simple
```

## Usage

## Example

```sh
$ env
TEST_BOOL="false"
TEST_STRING="this is a string"
TEST_LIST1="this is a list"
TEST_LIST2="this,is,also,a,list"
TEST_SECRET1="this is a secret in an environment variable\n"
TEST_SECRET2_FILE="./secret.txt"
$ cat -e ./secret.txt
this is a secret in a file$
```

```python
>>> env.get_bool("TEST_BOOL")
False
>>> env.get_bool("MISSING")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
MissingEnvVariable: `MISSING` should be present in the environment.
>>> env.get_bool("MISSING", True)
True
>>> env.get_string("TEST_STRING")
"this is a string"
>>> env.get_list("TEST_LIST1")
["this", "is", "a", "list"]
>>> env.get_list("TEST_LIST2", separator=",")
["this", "is", "also", "a", "list"]
>>> env.get_secret("TEST_SECRET1")
"this is a secret in an environment variable"
>>> env.get_secret("TEST_SECRET1", strip_value=False)
"this is a secret in an environment variable\n"
>>> env.get_secret("TEST_SECRET2")
"this is a secret in a file"
```

### Reference

#### `env.get_bool(name, default=env.NO_DEFAULT)`

#### `env.get_string(name, default=env.NO_DEFAULT)`

#### `env.get_list(name, default=env.NO_DEFAULT, separator=None)`

#### `env.get_secret(name, default=env.NO_DEFAULT, strip_value=True)`
