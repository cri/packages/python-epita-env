from os import environ


class MissingEnvVariable(RuntimeError):
    def __init__(self, name):
        super().__init__(f"`{name}` should be present in the environment.")


FALSE_VALUES = ("0", "off", "false", "no", "")
NO_DEFAULT = object()
MISSING_ENV = object()


def ignore_missing():
    return environ.get("IGNORE_MISSING_VALUES", "") not in FALSE_VALUES


def _get_raw_value(name, default=NO_DEFAULT):
    alt_name = f"{name}_FILE"
    if name not in environ and alt_name not in environ and default is NO_DEFAULT:
        if ignore_missing():
            return MISSING_ENV
        raise MissingEnvVariable(name)
    if name in environ:
        return environ.get(name)
    if alt_name in environ:
        with open(environ.get(alt_name), "r") as f:
            return f.read()
    return default


def get_bool(name, default=NO_DEFAULT):
    value = _get_raw_value(name, default)
    if value is MISSING_ENV:
        return None
    if value == default:
        return value
    return value.lower() not in FALSE_VALUES


def get_string(name, default=NO_DEFAULT):
    value = _get_raw_value(name, default)
    if value is MISSING_ENV:
        return "dummy"
    return value


def get_int(name, default=NO_DEFAULT):
    value = _get_raw_value(name, default)
    if value is MISSING_ENV:
        return int()
    return int(value)


def get_list(name, default=NO_DEFAULT, separator=None):
    value = _get_raw_value(name, default)
    if value is MISSING_ENV:
        return []
    if value == default:
        return value
    return value.split(separator)


def get_secret(name, default=NO_DEFAULT, strip_value=True):
    value = get_string(name, default)
    if strip_value:
        value = value.strip()
    return value
